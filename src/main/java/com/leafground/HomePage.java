package com.leafground;


import java.time.Duration;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class HomePage {
	
	
	
	//just decided to write all the tests here
	String url="http://www.leafground.com";
	WebDriver driver;
	long implictWaitTime=10;
	long timeOutInSeconds=15;
	WebDriverWait wait;
	
	
	//paths
	String editTextLinkPath="//li/a[contains(@href,\"Edit.html\")]";
	
	
	
	@BeforeTest
	void setupDriver() {
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		
		wait = new WebDriverWait(driver,timeOutInSeconds);
		
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(implictWaitTime, TimeUnit.SECONDS);
		driver.navigate().to(url);
	}
	
	@Test
	void clickEditTextLink() {
		try {
			WebElement editTextLink=driver.findElement(By.xpath(editTextLinkPath));
			wait.until(ExpectedConditions.elementToBeClickable(editTextLink));
			editTextLink.click();
			Thread.sleep(3000);
		}
		catch(Exception e) {
			System.out.println(e);
		}
		
	}
	
	@Test(dependsOnMethods = "clickEditTextLink")
	void workWithEditFields() {
		try {
			
			//enter email address inside text field
			WebElement email=driver.findElement(By.id("email"));
			email.sendKeys("test@gmail.com");
			Thread.sleep(1000);
			
			//append a text and press tab
			WebElement append = driver.findElement(By.xpath("//input[@type='text' and @value='Append ']"));
			append.sendKeys("extra words",Keys.TAB);
			//append.sendKeys("extra words"+Keys.TAB); //this also works as above
			Thread.sleep(1000);
			
			//get default text entered in the text field
			WebElement defaultText = driver.findElement(By.name("username"));
			//System.out.println("getText() "+ defaultText.getText()); //this does not work in this case because there is no text element as sub element here
			System.out.println("getAttribute() "+ defaultText.getAttribute("value"));
			Thread.sleep(1000);
			
			//clear text of text field
			WebElement clearText = driver.findElement(By.xpath("//label[text()='Clear the text']/following-sibling::input[@type='text']"));
			clearText.clear();
			Thread.sleep(2000);
			
			
			//check disabled or not
			WebElement disabledText = driver.findElement(By.xpath("//label[contains(text(),'disabled')]/following-sibling::input[@type='text']"));
			boolean expected=true;
			boolean actual=Boolean.parseBoolean(disabledText.getAttribute("disabled")); //converts string to boolean
			System.out.println("disabled : "+actual);
			Assert.assertEquals(actual, expected);
			Thread.sleep(2000);
			
			//need to add automatic scoll when we are handling below elements of the current page
			
		}
		catch(Exception e) {
			System.out.println(e);
		}
		
	}
	
	
	
	
	@AfterTest
	void clear() {
		driver.quit();
	}
	
}
